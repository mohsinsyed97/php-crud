package com.android.phpcrud.activites;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.phpcrud.R;
import com.android.phpcrud.helpers.ApiConstants;
import com.android.phpcrud.helpers.RequestHandlerSingleton;
import com.android.phpcrud.helpers.SharedPrefSingleton;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEditTextFirstName, mEditTextLastName, mEditTextEmail, mEditTextPass;
    private Button mButtonSignUp, mButtonLogin;
    private ProgressDialog mProgressDialog;

    private String mFirstName, mLastName, mEmail, mPass;

    private static final String LOG_TAG = SignUpActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mEditTextFirstName = (EditText) findViewById(R.id.et_first_name);
        mEditTextLastName = (EditText) findViewById(R.id.et_last_name);
        mEditTextEmail = (EditText) findViewById(R.id.et_email);
        mEditTextPass = (EditText) findViewById(R.id.et_pass);
        mButtonSignUp = (Button) findViewById(R.id.btn_sign_up);
        mButtonLogin = (Button) findViewById(R.id.btn_login);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);

        mButtonLogin.setOnClickListener(this);
        mButtonSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSignUp) {
            onClickSignUp();
        }
        if (v == mButtonLogin) {
            startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
        }
    }

    private void onClickSignUp() {
        if (isValid()) {
            mProgressDialog.setMessage("Creating account...");
            mProgressDialog.show();

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    signUp();
                    return null;
                }
            }.execute();
        }
    }

    private boolean isValid() {
        boolean flag = false;

        mFirstName = mEditTextFirstName.getText().toString().trim();
        mLastName = mEditTextLastName.getText().toString().trim();
        mEmail = mEditTextEmail.getText().toString().trim();
        mPass = mEditTextPass.getText().toString().trim();

        if (mFirstName.isEmpty()) {
            Toast.makeText(this, "First name is required", Toast.LENGTH_SHORT).show();
        } else if (mLastName.isEmpty()) {
            Toast.makeText(this, "Last name is required", Toast.LENGTH_SHORT).show();
        } else if (mEmail.isEmpty()) {
            Toast.makeText(this, "Email address is required", Toast.LENGTH_SHORT).show();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            Toast.makeText(this, "Email address is badly formatted", Toast.LENGTH_SHORT).show();
        } else if (mPass.isEmpty()) {
            Toast.makeText(this, "Password is required", Toast.LENGTH_SHORT).show();
        } else if (mPass.length() < 8) {
            Toast.makeText(this, "Password must be at least 8 characters long", Toast.LENGTH_SHORT).show();
        } else {
            flag = true;
        }
        return flag;
    }

    private void signUp() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_SIGN_UP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        Toast.makeText(SignUpActivity.this, "Your account has been successfully created\n\n" +
                                rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();

                        // Save user data to shared preferences
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        String id = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_USER_ID);
                        String fn = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_USER_FIRST_NAME);
                        String ln = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_USER_LAST_NAME);
                        String email = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_USER_EMAIL);
                        String password = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_USER_PASSWORD);

                        SharedPrefSingleton.getInstance(SignUpActivity.this).saveLoggedInUserData(id, fn, ln, email, password);

                        startActivity(new Intent(SignUpActivity.this, ProfileActivity.class));
                    } else {
                        Toast.makeText(SignUpActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    Toast.makeText(SignUpActivity.this, "Sorry, something went wrong. Please try again", Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.getMessage());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_USER_FIRST_NAME, mFirstName);
                params.put(ApiConstants.PARAM_USER_LAST_NAME, mLastName);
                params.put(ApiConstants.PARAM_USER_EMAIL, mEmail);
                params.put(ApiConstants.PARAM_USER_PASSWORD, mPass);
                return params;
            }
        };

        RequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
