package com.android.phpcrud.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SharedPrefSingleton {

    private static SharedPrefSingleton sInstance;
    private static Context sContext;

    private SharedPrefSingleton(Context context) {
        sContext = context;
    }

    public static synchronized SharedPrefSingleton getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SharedPrefSingleton(context);
        }
        return sInstance;
    }

    public void saveLoggedInUserData(String id, String firstName, String lastName, String email, String password) {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.SHARED_PREF_KEY_USER_ID, id);
        editor.putString(Constants.SHARED_PREF_KEY_USER_FIRST_NAME, firstName);
        editor.putString(Constants.SHARED_PREF_KEY_USER_LAST_NAME, lastName);
        editor.putString(Constants.SHARED_PREF_KEY_USER_EMAIL, email);
        editor.putString(Constants.SHARED_PREF_KEY_USER_PASSWORD, password);
        editor.apply();
    }

    public boolean isUserLoggedIn() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if (preferences.getString(Constants.SHARED_PREF_KEY_USER_ID, null) != null) {
            return true;
        }
        return false;
    }

    public void clearLoggedInUserData() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public HashMap<String, String> getLoggedInUserData() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        HashMap<String, String> userData = new HashMap<>();
        userData.put(Constants.SHARED_PREF_KEY_USER_ID, preferences.getString(Constants.SHARED_PREF_KEY_USER_ID, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_FIRST_NAME, preferences.getString(Constants.SHARED_PREF_KEY_USER_FIRST_NAME, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_LAST_NAME, preferences.getString(Constants.SHARED_PREF_KEY_USER_LAST_NAME, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_EMAIL, preferences.getString(Constants.SHARED_PREF_KEY_USER_EMAIL, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_PASSWORD, preferences.getString(Constants.SHARED_PREF_KEY_USER_PASSWORD, null));
        return userData;
    }
}
