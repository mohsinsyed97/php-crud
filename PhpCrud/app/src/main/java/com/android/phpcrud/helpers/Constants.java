package com.android.phpcrud.helpers;

public class Constants {

    // Shared pref constants
    public static final String SHARED_PREF_NAME = "my-shared-pref";
    public static final String SHARED_PREF_KEY_USER_ID = "user-id";
    public static final String SHARED_PREF_KEY_USER_FIRST_NAME = "user-first-name";
    public static final String SHARED_PREF_KEY_USER_LAST_NAME = "user-last-name";
    public static final String SHARED_PREF_KEY_USER_EMAIL = "user-email";
    public static final String SHARED_PREF_KEY_USER_PASSWORD = "user-password";
}
