package com.android.phpcrud.helpers;

public class ApiConstants {

    // Base URL
    private static final String BASE_URL = "http://192.168.1.103/crud/v1/";

    // JSON response keys
    public static final String KEY_STATUS = "status";
    public static final String KEY_RESPONSE = "response";

    public static final String STATUS_SUCCESS = "success";
    public static final String STATUS_ERROR = "error";

    // POST Params
    public static final String PARAM_USER_ID = "id";
    public static final String PARAM_USER_FIRST_NAME = "first_name";
    public static final String PARAM_USER_LAST_NAME = "last_name";
    public static final String PARAM_USER_EMAIL = "email";
    public static final String PARAM_USER_PASSWORD = "password";

    // API URLs
    public static final String API_SIGN_UP = BASE_URL + "create-user.php";
    public static final String API_LOGIN = BASE_URL + "login-user.php";
    public static final String API_UPDATE_PROFILE = BASE_URL + "update-profile-user.php";
    public static final String API_DELETE_ACCOUNT = BASE_URL + "delete-account-user.php";
}

