package com.android.phpcrud.activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.android.phpcrud.R;
import com.android.phpcrud.helpers.SharedPrefSingleton;

public class WelcomeActivity extends AppCompatActivity {

    private Button mButtonLogin, mButtonCreateAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        if (SharedPrefSingleton.getInstance(this).isUserLoggedIn()) {
            startActivity(new Intent(WelcomeActivity.this, ProfileActivity.class));
            finish();
            return;
        }
        mButtonLogin = (Button) findViewById(R.id.btn_login);
        mButtonCreateAccount = (Button) findViewById(R.id.btn_create_account);

        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
            }
        });

        mButtonCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WelcomeActivity.this, SignUpActivity.class));
            }
        });
    }
}
