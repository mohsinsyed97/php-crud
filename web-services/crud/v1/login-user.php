<?php 

    // array for JSON response
    $response = array();

    // check if the http request method is correct i.e. POST
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        // check if the required field(s) are not empty
        if (isset($_POST['email']) and isset($_POST['password'])) {
            
            // store the values of POST data
            $email = $_POST['email'];
            $pass = $_POST['password'];
        
            // include the user.php class file
            include_once ("../includes/user.php");
    
            // create new user object
            $user = new User();
            
            $res = $user->login($email, $pass);

            // check if the login is successful
            if ($res == 1) {
                $response['status'] = "success";
                $response['response'] = $user->read($email);
            }
            else if ($res == 0) {
                $response['status'] = "error";
                $response['response'] = "There is no user corresponding to this email address";
            }
            else if ($res == -1) {
                $response['status'] = "error";
                $response['response'] = "Incorrect password or the user does not have a password";
            }
        }
        else {
            $response['status'] = "error";
            $response['response'] = "Required parameters (email & password) are missing or empty";
        }
    }
    else {
        $response['status'] = "error";
        $response['response'] = "HTTP request method (POST) is missing";
    }

    echo json_encode($response);

?>