<?php 

    // array for JSON response
    $response = array();

    // include the user.php class file
    include_once ("../includes/user.php");    
    
    // create new user object
    $user = new User();

    // check if the http request method is POST
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        // check if the required field(s) are not empty
        if (isset($_POST['email'])) {

            // store the value of POST data
            $email = $_POST['email'];
            
            $res = $user->read($email);

            if ($res == 0) {
                $response['status'] = "error";
                $response['response'] = "There is no user corresponding to this email address";
            }
            else {
                $response['status'] = "success";
                $response['response'] = $res;
            }
        }
        else {
            $response['status'] = "error";
            $response['response'] = "Required parameter (email) is missing or empty";
        }
    }
    // check if the http request method is GET
    else if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $res = $user->read("");

        $response['status'] = "success";
        $response['response'] = $res;
    }
    else {
        $response['status'] = "error";
        $response['response'] = "HTTP request method (POST or GET) is missing";
    }

    echo json_encode($response);

?>