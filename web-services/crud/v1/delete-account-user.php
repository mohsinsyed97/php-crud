<?php 

    // array for JSON response
    $response = array();

    // check if the http request method is correct i.e. POST
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        // check if the required field(s) are not empty
        if (isset($_POST['id'])) {
            
            // store the values of POST data
            $id = $_POST['id'];
        
            // include the user.php class file
            include_once ("../includes/user.php");
    
            // create new user object
            $user = new User();
            
            $res = $user->delete($id);

            // check if the user is successfully deleted
            if ($res == 1) {
                $response['status'] = "success";
                $response['response'] = "Account successfully deleted";
            }
            else if ($res == 0) {
                $response['status'] = "error";
                $response['response'] = "No user found corresponding to the provided id";
            }
            else if ($res == -1) {
                $response['status'] = "error";
                $response['response'] = "Sorry, something went wrong. Please try again";
            }
        }
        else {
            $response['status'] = "error";
            $response['response'] = "Required parameter (id) is missing or empty";
        }
    }
    else {
        $response['status'] = "error";
        $response['response'] = "HTTP request method (POST) is missing";
    }

    echo json_encode($response);

?>