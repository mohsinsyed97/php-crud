<?php

    class User {

        private $con;

        function __construct() {
            require_once dirname(__FILE__).'/db-connect.php';
            $db = new DbConnect();
            $this->con = $db->connect();
        }

        public function create($first_name, $last_name, $email, $password) {
            if ($this->con->query("SELECT * FROM users WHERE email = '$email'")->num_rows > 0) {
                return 0; // User with the same email already exist
            }
            else {
                // $password = md5($password); // for password encryption
                $sql = "INSERT INTO users (first_name, last_name, email, password) VALUES ('$first_name', '$last_name', '$email', '$password')";

                if ($this->con->query($sql)) {
                    return 1; // Inserted successfully
                }
                else {
                    return -1; // Failed to insert
                }
            }
        }

        public function read($email) {
            $result;
            if ($email == '') {
                $result = $this->con->query("SELECT * FROM users ORDER BY created_at");
            }
            else {
                if ($this->con->query("SELECT * FROM users WHERE email = '$email'")->num_rows > 0) {
                    $result = $this->con->query("SELECT * FROM users WHERE email = '$email'");
                }
                else {
                    return 0; // The user with this email does not exist in the database
                }
            }
            
            $arr = array();
            while($row = mysqli_fetch_assoc($result)) {
                $arr[] = $row;
            }
            return $arr;
        }

        public function update($id, $first_name, $last_name, $email, $password) {
            if ($this->con->query("SELECT * FROM users WHERE id = '$id'")->num_rows <= 0) {
                return -1; // No user found corresponding to the provided id
            }
            else if ($this->con->query("SELECT email FROM users WHERE email = '$email'")->num_rows > 0) {
                $result = $this->con->query("SELECT email FROM users WHERE id = '$id'");
                while($row = mysqli_fetch_assoc($result)) {
                    $userEmail = $row['email'];
                }
                if ($email === $userEmail) {
                    $sql = "UPDATE users SET first_name = '$first_name', last_name = '$last_name', email = '$email', password = '$password', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
                    if ($this->con->query($sql)) {
                        return 1; // Updated successfully
                    }
                    else {
                        return -2; // Failed to update
                    }
                }
                else {
                    return -3; // Email already exist in the database
                }
            }
            else {
                $sql = "UPDATE users SET first_name = '$first_name', last_name = '$last_name', email = '$email', password = '$password', modified_at = CURRENT_TIMESTAMP() WHERE id = '$id'";
                if ($this->con->query($sql)) {
                    return 1; // Updated successfully
                }
                else {
                    return -2; // Failed to update
                }
            }
        }

        public function delete($id) {
            if ($this->con->query("SELECT * FROM users WHERE id = '$id'")->num_rows <= 0) {
                return 0; // No user found corresponding to the provided id
            }
            else {
                $sql = "DELETE FROM users WHERE id = '$id'";
                if ($this->con->query($sql)) {
                    return 1; // Deleted successfully
                }
                else {
                    return -1; // Failed to delete
                }
            }
        }

        public function login($email, $password) {
            if ($this->con->query("SELECT * FROM users WHERE email = '$email'")->num_rows <= 0) {
                return 0; // The user with this email does not exist in the database
            }
            else {
                if ($this->con->query("SELECT * FROM users WHERE email = '$email' and password = '$password'")->num_rows > 0) {
                    return 1; // Login successful
                }
                else {
                    return -1; // Incorrect password or the user does not have a password
                }
            }
        }
    }

?>